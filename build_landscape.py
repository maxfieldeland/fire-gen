#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 27 14:13:11 2020

@author: max

script to build/downsize landscapes



"""

import numpy as np
import glob 
import re
import matplotlib.pyplot as plt

N_CHANNELS = 5

#load in king_fire data layers

fire_name = 'king_fire'

elevations = np.load(fire_name+"/preprocessed/topography.npy")
red = np.load(fire_name+"/preprocessed/red_layer.npy")
blue = np.load(fire_name+"/preprocessed/blue_layer.npy")
green = np.load(fire_name+"/preprocessed/green_layer.npy")
infred = np.load(fire_name+'/preprocessed/infred_layer.npy')

channel_dim = elevations.shape
file_paths = glob.glob(fire_name + '/preprocessed/burn_*.npy')
file_paths.sort(key=lambda f: int(re.sub('\D', '', f)))

channel_width = elevations.shape[0]
landscape_time_series = np.zeros([5,channel_width, channel_width,N_CHANNELS])

for idx,file in enumerate(file_paths[:5]):
    if idx < len(file_paths)-1:
        X = np.load(file)
        # the y state map is one time step ahead
        y = np.load(file_paths[idx+1])
        channels_X = np.zeros([channel_dim[0],channel_dim[1],N_CHANNELS])
        channels_X[:,:,0] = elevations
        channels_X[:,:,1] = X
        channels_X[:,:,2] = blue
        channels_X[:,:,3] = green
        channels_X[:,:,4] = infred     
        landscape_time_series[idx, :,:,:] = channels_X
        
        
burn1 = landscape_time_series[0]
np.save('sim_data_king/king_burn_1',burn1)
burn2 = landscape_time_series[1]
np.save('sim_data_king/king_burn_2',burn2)
burn3 = landscape_time_series[2]
np.save('sim_data_king/king_burn_3',burn3)
burn4 = landscape_time_series[3]
np.save('sim_data_king/king_burn_4',burn4)
burn5 = landscape_time_series[4]
np.save('sim_data_king/king_burn_5',burn5)



